
@extends('layouts.plantillabase')

@section('nav')

@endsection

@section('contenido')

    <div class="container mt-5">
        
    <div class="row">
            <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Usuarios</h2>
                    </div>
                    <div class="pull-right mb-2">
                        <a class="btn btn-success" href="{{ route('usuarios.create') }}">Crear usuarios</a>
                    </div>
            </div>
    </div>
    
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    
    <table id="usuario" class="table table-bordered"  style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Identifivcacion</th>
                <th>Usuario</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($usuarios as $usuario)
                <tr>
                    <td>{{ $usuario->id }}</td>
                    <td>{{ $usuario->nombre }}</td>
                    <td>{{ $usuario->codigo }}</td>
                    <td>{{ $usuario->usuario }}</td>
                    <td>
                        <form action="{{ route('usuarios.destroy',$usuario->id) }}" method="Post">
                            <a class="btn btn-primary" href="{{ route('usuarios.edit',$usuario->id) }}">Editar</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Eliminar</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="pull-right">
        <a class="btn btn-primary "href="{{route('inicio')}}">Regresar al inicio</a>
    </div>
@endsection