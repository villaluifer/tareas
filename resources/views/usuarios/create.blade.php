@extends('layouts.plantillabase')
 
@section('contenido')

    <div class="container mt-2">

        <div class="row">
            <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Crear usuario</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('usuarios.index') }}" enctype="multipart/form-data"> Regresar</a>
                    </div>
            </div>
        </div>
        
    @if(session('status'))
        <div class="alert alert-success mb-1 mt-1">
            {{ session('status') }}
        </div>
    @endif
    
        <form action="{{ route('usuarios.store')}}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Nombre:</strong>
                        <input type="text" name="nombre" class="form-control" placeholder="nombre">
                        @error('nombre')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>identificacion:</strong>
                        <input type="text" name="codigo" class="form-control" placeholder="identificacion">
                        @error('codigo')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
               <div>
                <button type="submit" class="btn btn-primary mr-3">Enviar</button>
               </div>
            </div>
        </form>
    </div>

@endsection