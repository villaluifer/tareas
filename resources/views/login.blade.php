<!doctype html>
<html lang="en">

<head>
  <title>Incio Sesión</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS v5.2.1 -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link href="{{asset('css/style.css')}}" rel="stylesheet" >
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>

<body>
  
<section class="h-100 gradient-form">
  <div class="container h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-xl-10">
        <div class="card rounded-3 text-black">
          <div class="row g-0">
            <div class="col-lg-6">
              <div class="card-body p-md-5 mx-md-4">
                {{-- @dump($errors->all()) --}}
                <form id="form" action="{{route('login.post')}}" method="POST">
                  <p class="text-center">Ingresa tus datos de usuario</p>
                  @csrf

                <div>
                  <input type="hidden"  name="mensaje" id="mensaje" class="form-control" value="{{old('mensaje')}}">
                  @error('mensaje')
                  <strong><small style="color: red">{{$message}}</small></strong>
                  @enderror
                </div>

                  <div class="form-outline mb-4">
                    <label class="form-label" for="usuario">Usuario</label>
                    <input name="usuario" id="usuario" class="form-control" value="{{old('usuario')}}" placeholder="Ingrese identificacion"/>
                    @error('usuario')
                     <div class="alert alert-danger mt-1 mb-1"><strong><small>{{$message}}</small></strong></div>
                    @enderror
                    <span class="badge text-danger errors-usuario"></span>
                  </div>

                  <div class="form-outline mb-4">
                    <label class="form-label" for="password">Contraseña</label>
                    <input type="password" name="password" id="password" class="form-control"
                     value="{{old('password')}}" placeholder="Ingrese una contraseña"/>
                    @error('password')
                    <div class="alert alert-danger mt-1 mb-1"><strong><small>{{$message}}</small></strong></div>
                    @enderror
                    <span class="badge text-danger errors-password"></span>
                  </div>

                  @if ($errors->has('login'))
                    <div class="alert alert-danger">
                        <ul>
                          @error('login')
                          <Strong><small style="color: red">{{$message}}</small></Strong>
                          @enderror
                        </ul>
                    </div>
                  @endif

                  <div style="text-align: center">
                    <a class="text-muted" href="#!" style="text-decoration:none;">¿Olvidaste tu Contraseña?</a>
                  </div>
                  <br>
                  <div class="text-center mb-4 pt-1">
                      <button class="btn btn-primary btn-block fa-lg mb-3"type="text" id="btn-inciar">
                        Iniciar sesion</button>
                  </div>
                  <hr class="pt-3">
                  <div class="d-flex align-items-center justify-content-center pb-2">
                   <a href="{{route('register')}}" style="text-decoration:none; color:#002392;"><b>Registrate</b></a>
                  </div>
                </form>
              </div>
            </div>
            <div class="col-lg-6 d-flex align-items-center">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

  {{-- <script type="text/javascript" src="{{asset('js/login.js')}}"></script> --}}

  <!-- Bootstrap JavaScript Libraries -->
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
    integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
  </script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js"
    integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous">
  </script>
</body>

</html>