
@extends('layouts.plantillabase')

@section('nav')

@endsection

@section('contenido')

    <div class="container mt-5">
        
    <div class="row">
            <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Etiqueta</h2>
                    </div>
                    <div class="pull-right mb-2">
                        <a class="btn btn-success" href="{{ route('etiquetas.create') }}">Crear Etiqueta</a>
                    </div>
            </div>
    </div>
    
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    
    <table id="etiquetas" class="table table-bordered"  style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre etiqueta</th>
                <th>Cod tarea</th>
                <th>Codigo</th>
                <th>Descripcion</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($etiquetas as $etiqueta)
                <tr>
                    <td>{{ $etiqueta->id }}</td>
                    <td>{{ $etiqueta->nombre }}</td>
                    <td>{{ $etiqueta->id_tarea }}</td>
                    <td>{{ $etiqueta->codigo }}</td>
                    <td>{{ $etiqueta->descripcion }}</td>
                    <td>
                        <form action="{{ route('etiquetas.destroy',$etiqueta->id) }}" method="Post">
                            <a class="btn btn-primary" href="{{ route('etiquetas.edit',$etiqueta->id) }}">Editar</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Eliminar</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="pull-right">
        <a class="btn btn-primary "href="{{route('inicio')}}">Regresar al inicio</a>
    </div>
@endsection