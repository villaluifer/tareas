@extends('layouts.plantillabase')
 
@section('contenido')

    <div class="container mt-2">

        <div class="row">
            <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Editar etiqueta</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('etiquetas.index') }}" enctype="multipart/form-data"> Regresar</a>
                    </div>
            </div>
        </div>
        
    @if(session('status'))
        <div class="alert alert-success mb-1 mt-1">
            {{ session('status') }}
        </div>
    @endif
    
        <form action="{{ route('etiquetas.update',$etiqueta->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Nombre etiqueta:</strong>
                        <input type="text" name="nombre" class="form-control" value="{{$etiqueta->nombre}}" placeholder="Nombre etiqueta">
                       @error('nombre')
                          <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                       @enderror
                    </div>
                </div>
        
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Asignar tarea:</strong>
                        <select name="id_tarea" id="id_tarea" class="form-control">
                            <option value=""></option>
                            @foreach ($tareas as $tarea)
                            <option value="{{$tarea->id}}">{{$tarea->nombre}}</option>
                            @endforeach
                         </select>
                        @error('tarea')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                    @enderror
                    </div>
                </div>
         
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Codigo:</strong>
                         <input type="text" name="codigo" value="{{$etiqueta->codigo}}" class="form-control" placeholder="codigo">
                        @error('codigo')
                          <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                       @enderror
                    </div>
                </div>
        
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Descripcion:</strong>
                         <input type="text" name="descripcion" value="{{$etiqueta->descripcion}}"  class="form-control" placeholder="Descripcion">
                        @error('descripcion')
                          <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                       @enderror
                    </div>
                </div>
                
                <div>
                    <button type="submit" class="btn btn-primary mr-3">Enviar</button>
                </div>
            </div>
        </form>
    </div>

@endsection