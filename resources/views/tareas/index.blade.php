
@extends('layouts.plantillabase')

@section('nav')

@endsection

@section('contenido')

    <div class="container mt-5">
        
    <div class="row">
            <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Tareas</h2>
                    </div>
                    <div class="pull-right mb-2">
                        <a class="btn btn-success" href="{{ route('tareas.create') }}">Crear tareas</a>
                    </div>
            </div>
    </div>
    
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    
    <table id="tareas" class="table table-bordered"  style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre Tarea</th>
                <th>Usuario</th>
                <th>descripcion</th>
                <th>fecha_creacion</th>
                <th>fecha_vencimiento</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tareas as $tarea)
                <tr>
                    <td>{{ $tarea->id }}</td>
                    <td>{{ $tarea->nombre }}</td>
                    <td>{{ $tarea->id_usuario}}</td>
                    <td>{{ $tarea->descripcion }}</td>
                    <td>{{ $tarea->fecha_creacion }}</td>
                    <td>{{ $tarea->fecha_vencimiento }}</td>
                    <td>
                        <form action="{{ route('tareas.destroy',$tarea->id) }}" method="Post">
                            <a class="btn btn-primary" href="{{ route('tareas.edit',$tarea->id) }}">Editar</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Eliminar</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="pull-right">
        <a class="btn btn-primary "href="{{route('inicio')}}">Regresar al inicio</a>
    </div>
@endsection