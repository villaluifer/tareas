@extends('layouts.plantillabase')
 
@section('contenido')

<div class="container mt-2">
   
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left mb-2">
            <h2>Agregar tarea</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary "href="{{route('tareas.index')}}"> Regresar</a>
        </div>
    </div>
</div>
    
  @if(session('status'))
    <div class="alert alert-success mb-1 mt-1">
        {{ session('status') }}
    </div>
  @endif
    
<form action="{{ route('tareas.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre tarea:</strong>
                <input type="text" name="nombre" class="form-control" placeholder="Nombre tarea">
               @error('nombre')
                  <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
               @enderror
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Usuario:</strong>
                <select name="id_usuario" id="id_usuario" class="form-control">
                    <option value=""></option>
                    @foreach ($usuarios as $usuario)
                    <option value="{{$usuario->id}}">{{$usuario->nombre}}</option>
                    @endforeach
                 </select>
                @error('usuario')
                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
            @enderror
            </div>
        </div>
 
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Descripcion:</strong>
                 <input type="text" name="descripcion" class="form-control" placeholder="Descripcion">
                @error('descripcion')
                  <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
               @enderror
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Fecha creación:</strong>
                 <input type="datetime" name="fecha_creacion" class="form-control">
                @error('fecha_creacion')
                  <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
               @enderror
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Fecha Vencimiento:</strong>
                 <input type="date" name="fecha_vencimiento" class="form-control"
                 value="" min="2023-01-01" max="2023-12-31">
                @error('fecha_vencimiento')
                  <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
               @enderror
            </div>
        </div>
        
        <div>
            <button type="submit" class="btn btn-primary mr-3">Enviar</button>
        </div>
    
</form>

@endsection