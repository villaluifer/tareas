@extends('layouts.plantillabase')
 
@section('contenido')

    <div class="container mt-2">

        <div class="row">
            <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Editar tarea</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('tareas.index') }}" enctype="multipart/form-data"> Regresar</a>
                    </div>
            </div>
        </div>
        
    @if(session('status'))
        <div class="alert alert-success mb-1 mt-1">
            {{ session('status') }}
        </div>
    @endif
    
        <form action="{{ route('tareas.update',$tarea->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            
            @error('render')
            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
           @enderror

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Nombre tarea:</strong>
                        <input type="text" name="nombre" value="{{$tarea->nombre}}" class="form-control" placeholder="Nombre tarea">
                       @error('nombre')
                          <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                       @enderror
                    </div>
                </div>
        
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Usuario:</strong>
                        <select name="id_usuario" id="id_usuario" class="form-control" value="{{$tarea->id_usuario}}">
                            <option></option>
                            @foreach ($usuarios as $usuario)
                            <option value="{{$usuario->id}}">{{$usuario->nombre}}</option>
                            @endforeach
                         </select>
                        @error('usuario')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                    @enderror
                    </div>
                </div>
         
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Descripcion:</strong>
                         <input type="text" name="descripcion" class="form-control" value="{{$tarea->descripcion}}" placeholder="Descripcion">
                        @error('descripcion')
                          <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                       @enderror
                    </div>
                </div>
        
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Fecha creación:</strong>
                         <input type="datetime" name="fecha_creacion" class="form-control" 
                         value="2023-09-11" min="2023-01-01" max="2023-12-31">
                        @error('fecha_creacion')
                          <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                       @enderror
                    </div>
                </div>
        
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Fecha Vencimiento:</strong>
                         <input type="date" name="fecha_vencimiento" class="form-control"
                         value="2023-09-11" min="2023-01-01" max="2023-12-31">
                        @error('fecha_vencimiento')
                          <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                       @enderror
                    </div>
                </div>

                
                @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
                @endif
                
                <div>
                    <button type="submit" class="btn btn-primary mr-3">Enviar</button>
                </div>

            </div>
        </form>
    </div>

@endsection