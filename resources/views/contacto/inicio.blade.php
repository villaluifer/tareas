<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @yield('css')
    <!-- Bootstrap CSS v5.2.1 -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
  integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <title></title>
</head>
<body>
 
  <nav class="navbar navbar-light bg-light">
    <div class="container-fluid">
      <span class="navbar-text">
        BIENVENIDOS
      </span>
      @auth
      <p id="lg">{{ Auth::User()->nombre }}</p>
    @endauth
      <a class="navbar-brand" href="{{route('logout')}}"><button type="button" class="btn-close" aria-label="Close"></button></a>
    </div>
  </nav>

<div class="container">

</div>
<div class="contendido">
  <a href="{{route('tareas.index')}}">Tareas</a>
</div>

<div class="contendido">
  <a href="{{route('etiquetas.index')}}">Etiquetas</a>
</div>

<div class="contendido">
  <a href="{{route('usuarios.index')}}">usuarios</a>
</div>

@yield('js')
 <!-- Bootstrap JavaScript Libraries -->
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
  integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js"
  integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous">
</script>


</body>
</html>