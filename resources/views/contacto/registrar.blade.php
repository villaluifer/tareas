<!doctype html>
<html lang="en">

<head>
  <title>Registrar</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS v5.2.1 -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link href="{{asset('css/style.css')}}" rel="stylesheet" >
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>

<body>
  
<section class="h-100 gradient-form" style="background-color: #eee;">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-xl-10">
        <div class="card rounded-3 text-black">
          <div class="row g-0">
            <div class="col-lg-6">
              <div class="card-body p-md-5 mx-md-4">
                 {{-- @dump($errors->all()) --}}
                <form action="{{route('registrar')}}" method="POST" name="frm-registro">
                  @csrf
                  
                  <div class="form-outline mb-4">
                    <label class="form-label" for="nombre">Nombre</label>
                    <input type="text" class="form-control" name="nombre" id="nombre" value="{{old('nombre')}}" placeholder="Ingresa tu nombre"/>
                    @error('nombre')
                   <strong><small style="color: red">{{$message}}</small></strong>
                   @enderror
                  </div>

                  <div class="form-outline mb-4">
                    <label class="form-label" for="codigo">Identificacion</label>
                    <input type="text" class="form-control" name="codigo" id="codigo" value="{{old('codigo')}}" placeholder="Ingresar identificacion"/>
                    @error('codigo')
                    <strong><small style="color: red">{{$message}}</small></strong>
                    @enderror
                  </div>


                  <div class="form-outline mb-4 ">
                    <label class="form-label" for="password">Contraseña</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Ingresa una contraseña"/>
                    @error('password')
                    <strong><small style="color: red">{{$message}}</small></strong>
                    @enderror
                  </div>

                   <div class="form-outline mb-4 ">
                    <label class="form-label" for="password_confirmation">Repetir Contraseña</label>
                    <input type="password" class="form-control" name="password_confirmation"
                    id="password_confirmationp" placeholder="Ingrese nuevamente la contraseña"/>
                  </div>

                  <div class="d-flex align-items-center justify-content-center pb-4">
                  <button type="text" class="btn btn-primary btn-block" id="btn-registrar">
                    Registrate</button>&nbsp;
                    <a href="{{route('login')}}" class="btn btn-primary btn-block">Regresar</a>
                  </div>

                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

{{-- <script type="text/javascript" src="{{asset('js/login.js')}}"></script> --}}

  <!-- Bootstrap JavaScript Libraries -->
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
    integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
  </script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js"
    integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous">
  </script>
</body>

</html>