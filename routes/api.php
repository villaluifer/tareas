<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('register', '\App\Http\Controllers\UserController@register');
Route::post('login', '\App\Http\Controllers\UserController@authenticate');

$idtarea ='tareas/{id}';
    Route::get('tareas', 'App\Http\Controllers\TareaController@index');
    Route::get($idtarea, 'App\Http\Controllers\TareaController@show');
    Route::post('tareas', 'App\Http\Controllers\TareaController@store');
    Route::put($idtarea, 'App\Http\Controllers\TareaController@update');
    Route::delete($idtarea, 'App\Http\Controllers\TareaController@delete');

Route::group(['middleware' => ['jwt.verify']], function() {

    $idusuario ='usuarios/{id}';
    Route::get('usuarios', 'App\Http\Controllers\UsuarioController@index');
    Route::get($idusuario, 'App\Http\Controllers\UsuarioController@show');
    Route::post('usuarios', 'App\Http\Controllers\UsuarioController@store');
    Route::put($idusuario, 'App\Http\Controllers\UsuarioController@update');
    Route::delete($idusuario, 'App\Http\Controllers\UsuarioController@delete');

    $idetiqueta ='etiquetas/{id}';
    Route::get('etiquetas', 'App\Http\Controllers\EtiquetaController@index');
    Route::get($idetiqueta, 'App\Http\Controllers\EtiquetaController@show');
    Route::post('etiquetas', 'App\Http\Controllers\EtiquetaController@store');
    Route::put($idetiqueta, 'App\Http\Controllers\EtiquetaController@update');
    Route::delete($idetiqueta, 'App\Http\Controllers\EtiquetaController@delete');

});