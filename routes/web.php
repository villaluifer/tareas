<?php

use App\Http\Controllers\EtiquetaCrudController;
use App\Http\Controllers\TareaCrudController;
use App\Http\Controllers\UsuarioCrudController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


    Route::view('/',"login")->name('login');

    Route::view('/inicio',"inicio")->middleware('inicio');
    Route::view('/login',"login")->name('login');
    Route::view('/registrar',"registrar")->name('registrar');


    Route::get('/Showregister', 'App\Http\Controllers\LoginController@showRegistrar')->name('register');
    Route::post('/registrar', 'App\Http\Controllers\LoginController@register')->name('registrar');
    Route::get('/inicio','App\Http\Controllers\LoginController@inicio')->name('inicio');

    Route::post('/validarLogin','App\Http\Controllers\LoginController@validarLogin');

    Route::post('/login', 'App\Http\Controllers\LoginController@login')->name('login.post');
    Route::get('/logout', 'App\Http\Controllers\LoginController@logout')->name('logout');

    Route::resource('tareas', TareaCrudController::class);
    Route::resource('etiquetas', EtiquetaCrudController::class);
    Route::resource('usuarios', UsuarioCrudController::class);

