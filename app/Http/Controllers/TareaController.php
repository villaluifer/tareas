<?php

namespace App\Http\Controllers;

use App\Models\Tarea;
use Illuminate\Http\Request;


/**
* @OA\Server(url="http://pttareas.test")
*/


class TareaController extends Controller
{
    /**
    * @OA\SecurityScheme(
    *     type="http",
    *     description="Autenticación JWT",
    *     name="Authorization",
    *     in="header",
    *     scheme="bearer",
    *     bearerFormat="JWT",
    *     securityScheme="bearerAuth"
    * )
    */

    //Controlador ruta Api

    /**
    * @OA\Get(
    *     path="/api/tareas",
    *       tags={"Tareas"},
    *       summary="Listado del registro de todas las tareas",
    *     @OA\Response(
    *         response=200,
    *         description="Mostrar todos los proyectos."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     ),
    *     security={
    *        {"bearerAuth": {}},
    *     },
    * )
    */

    public function index(){
        return Tarea::all();
    }

    /**
    * @OA\Get(
    *     path="/api/tareas/{id}",
    *       tags={"Tareas"},
    *       summary="Listado del registro de todas las tareas",
     *     @OA\Parameter(
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Mostrar todos los proyectos."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     ),
    *     security={
    *        {"bearerAuth": {}},
    *     },
    * )
    */
    public function show($id){
        return Tarea::find($id);
    }

 /**
    * @OA\Post(
    *     path="/api/tareas",
    *     tags={"Tareas"},
    *     summary="Crear tarea",
    *     @OA\RequestBody(
    *         @OA\JsonContent(
    *            @OA\Property(property="id", type="number"),
    *            @OA\Property(property="nombre", type="string"),
    *            @OA\Property(property="id_usuario", type="number"),
    *            @OA\Property(property="descripcion", type="string"),
    *            @OA\Property(property="fecha_creacion", type="string"),
    *            @OA\Property(property="fecha_vencimiento", type="string")
    *         )
    *     ),
    *     @OA\Response(
    *         response=201,
    *         description="Proyecto creado satisfactoriamente."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function store(Request $request){
        Tarea::create($request->all());
    }

 /**
    * @OA\Put(
    *     path="/api/tareas/{id}",
    *     tags={"Tareas"},
    *     summary="Actualizar Tarea",
    *     @OA\RequestBody(
    *         @OA\JsonContent(
    *         @OA\Property(property="id", type="number"),
    *            @OA\Property(property="nombre", type="string"),
    *            @OA\Property(property="id_usuario", type="number"),
    *            @OA\Property(property="descripcion", type="string"),
    *            @OA\Property(property="fecha_creacion", type="string"),
    *            @OA\Property(property="fecha_vencimiento", type="string")
    *         )
    *     ),
    *     @OA\Response(
    *         response=202,
    *         description="Proyecto actualizado satisfactoriamente."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function update(Request $request, $id){
        $tarea = Tarea::findOrFail($id);
        $tarea->update($request->all());
        return $tarea;
    }

    /**
    * @OA\Delete(
    *     path="/api/tareas/{id}",
    *     tags={"Tareas"},
    *     summary="Eliminar Tarea",
    *     @OA\Response(
    *         response=204,
    *         description="Proyecto eliminado satisfactoriamente."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function delete($id){
        $tarea = Tarea::findOrFail($id);
        $tarea->delete();
        return 204;
    }
}
