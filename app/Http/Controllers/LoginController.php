<?php 

namespace App\Http\Controllers;

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Usuario;
use Illuminate\Support\Facades\Hash;


class LoginController extends Controller
{

    public function register(Request $request){

        $request->validate([
            'nombre' => 'required',
            'codigo' => 'required|unique:usuarios',
            'password' => 'required|string|min:6|confirmed',
    ]);
        Usuario::create([
        'nombre' => $request->get('nombre'),
        'codigo'=> $request->get('codigo'),
        'usuario' => $request->get('codigo'),
        'password_app' => sha1(md5($request->get('usuario').$request->get('password'))),
        'password' => Hash::make($request->get('password')),
        'password_confirmation'=>Hash::make($request->get('password'))
        ]);
            return Redirect(route('login'));
    }

    public function login (Request $request){

            $request->validate([
                'usuario'=>'required',
                'password'=>'required',
            ],[
                'usuario.required'=>'Usuario requerido*',
                'password.required'=>'Password requerido*',
            ]);
    
            $usuario = $request->get('usuario');
            $password = $request->get('password');
            
            $credentials = ['usuario' => $usuario, 'password' => $password];
    
                if(Auth::attempt($credentials)){
       
                    $request->session()->regenerate();
                    return redirect()->route('inicio');
                }else{
                    return redirect()->back()->withErrors([
                        'login' => 'Las credenciales proporcionadas son incorrectas',
                    ]);
                }
    }

    public function logout(Request $request){
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect(route('login'));
    }

    public function showRegistrar(){
        return view('contacto.registrar');
    }

    public function inicio(){
        return view('contacto.inicio');
    }
}
?>