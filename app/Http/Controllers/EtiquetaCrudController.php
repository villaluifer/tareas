<?php

namespace App\Http\Controllers;

use App\Models\Etiqueta;
use App\Models\Tarea;
use Illuminate\Http\Request;

class EtiquetaCrudController extends Controller
{
    // Controlador ruta web

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function index()
     {
         $etiquetas = Etiqueta::all();
         return view('etiquetas.index', Etiqueta::orderBy('id','desc')->paginate(5),compact('etiquetas'));
     }
 
      /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
 
     public function create()
     {
         $tareas = Tarea::all();
         return view('etiquetas.create', compact('tareas'));
     }
 
     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
 
      public function store(Request $request)
     {
         $request->validate([
             'nombre' => 'required',
             'id_tarea' => 'required',
             'codigo' => 'required',
             'descripcion' => ''
         ]);
  
         $etiqueta = new Etiqueta;
  
         $etiqueta->nombre = $request->nombre;
         $etiqueta->id_tarea = $request->id_tarea;
         $etiqueta->codigo = $request->codigo;
         $etiqueta->descripcion = $request->descripcion;
 
         $etiqueta->save();
  
       
         return redirect()->route('etiquetas.index')
                         ->with('success','La etiqueta ha sido creada con exito.');
     }
 
     /**
      * Display the specified resource.
      *
      * @param  \App\Etiqueta  $etiqueta
      * @return \Illuminate\Http\Response
      */
 
      public function show(Etiqueta $etiqueta)
     {
         return view('etiquetas.show',compact('etiqueta'));
     }
     
      /**
      * Show the form for editing the specified resource.
      *
      * @param  \App\Etiqueta  $etiqueta
      * @return \Illuminate\Http\Response
      */
 
      public function edit(Etiqueta $etiqueta)
      {
          $tareas = Tarea::all();
          return view('etiquetas.edit',compact('etiqueta'),compact('tareas'));
      }
 
      /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  \App\Etiqueta  $etiqueta
      * @return \Illuminate\Http\Response
      */
 
      public function update(Request $request, $id)
     {
        $request->validate([
            'nombre' => 'required',
            'id_tarea' => 'required',
            'codigo' => 'required',
            'descripcion' => ''
        ]);
  
         $etiqueta = Etiqueta::find($id);
  
         $etiqueta->nombre = $request->nombre;
         $etiqueta->id_tarea = $request->id_tarea;
         $etiqueta->codigo = $request->codigo;
         $etiqueta->descripcion = $request->descripcion;
 
         $etiqueta->save();
      
         return redirect()->route('etiquetas.index')
                         ->with('success','La etiqueta ha sido actualizada con exito');
     }
 
 
      /**
      * Remove the specified resource from storage.
      *
      * @param  \App\Etiqueta  $etiqueta
      * @return \Illuminate\Http\Response
      */
 
      public function destroy(Etiqueta $etiqueta)
     {
         $etiqueta->delete();
      
         return redirect()->route('etiquetas.index')
                         ->with('success','La etiqueta ha sido eliminada con exito');
     }
}
