<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

/**
* @OA\Server(url="http://pttareas.test")
*/

class UserController extends Controller

{

    /**
    * @OA\SecurityScheme(
    *     type="http",
    *     description="Autenticación JWT",
    *     name="Authorization",
    *     in="header",
    *     scheme="bearer",
    *     bearerFormat="JWT",
    *     securityScheme="bearerAuth"
    * )
    */


    /**
    * @OA\Post(
    *     path="/api/login",
    *     tags={"Auth"},
    *     summary="Atenticación",
    *     @OA\RequestBody(
    *         @OA\JsonContent(
    *            @OA\Property(property="usuario", type="string"),
    *            @OA\Property(property="password", type="string")
    *         )
    *     ),
    *     @OA\Response(
    *         response=201,
    *         description="Token obtenido"
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     ),
    *     security={
    *        {"bearerAuth": {}},
    *     },
    * )
    */
    public function authenticate(Request $request) {
        $credentials = $request->only('usuario', 'password');
            try {
                if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
                }
            } catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token'], 500);
            }
        return response()->json(compact('token'));
    }


    public function register(Request $request){
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|max:255',
            'codigo' => 'required|string|max:255|unique:usuarios',
            'usuario' => 'required|string|max:255',
            'password' => 'required|string|min:6|confirmed',

        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = User::create([
            'nombre' => $request->get('nombre'),
            'codigo' => $request->get('codigo'),
            'usuario' => $request->get('codigo'),
            'password_app' => sha1(md5($request->get('usuario').$request->get('password'))),
            'password' => Hash::make($request->get('password')),
        ]);
        $token = JWTAuth::fromUSer($user);
        return response()->json(compact('user', 'token'), 201);
    }

    public function getAuthenticatedUser(){
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        return response()->json(compact('user'));
    }
}
