<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Http\Request;

/**
* @OA\Server(url="http://pttareas.test")
*/

class UsuarioController extends Controller
{

/**
    * @OA\SecurityScheme(
    *     type="http",
    *     description="Autenticación JWT",
    *     name="Authorization",
    *     in="header",
    *     scheme="bearer",
    *     bearerFormat="JWT",
    *     securityScheme="bearerAuth"
    * )
    */

    // Controlador ruta Api

   // http://pttareas.test/api/documentation#/

     /**
    * @OA\Get(
    *     path="/api/usuarios",
    *       tags={"Usuarios"},
    *       summary="Listado del registro de todos los usuarios",
    *       security={ {"bearerAuth":{}}},
    *     @OA\Response(
    *         response=200,
    *         description="Mostrar todos los proyectos."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     ),
    * )
    */
    public function index(){
        return Usuario::all();
    }

    /**
    * @OA\Get(
    *     path="/api/usuarios/{id}",
    *       tags={"Usuarios"},
    *       summary="Listado un usuario",
     *      @OA\Parameter(
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Mostrar todos los proyectos."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     ),
    *     security={
    *        {"bearerAuth":{}},
    *     },
    * )
    */
    public function show($id){
        return Usuario::find($id);
    }

    /**
    * @OA\Post(
    *     path="/api/usuarios",
    *     tags={"Usuarios"},
    *     summary="Crear usuario",
    *     @OA\RequestBody(
    *         @OA\JsonContent(
    *            @OA\Property(property="id", type="number"),
    *            @OA\Property(property="nombre", type="string"),
    *            @OA\Property(property="codigo", type="string"),
    *            @OA\Property(property="descripcion", type="string"),
    *            @OA\Property(property="usuario", type="string")
    *         )
    *     ),
    *     @OA\Response(
    *         response=201,
    *         description="Proyecto creado satisfactoriamente."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function store(Request $request){
        Usuario::create($request->all());
    }

    /**
    * @OA\Put(
    *     path="/api/usuarios{id}",
    *     tags={"Usuarios"},
    *     summary="Actualizar usuario",
    *     @OA\RequestBody(
    *         @OA\JsonContent(
    *            @OA\Property(property="id", type="number"),
    *            @OA\Property(property="nombre", type="string"),
    *            @OA\Property(property="codigo", type="string"),
    *            @OA\Property(property="descripcion", type="string"),
    *            @OA\Property(property="usuario", type="string")
    *         )
    *     ),
    *     @OA\Response(
    *         response=202,
    *         description="Proyecto creado satisfactoriamente."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function update(Request $request, $id){
        $usuario = Usuario::findOrFail($id);
        $usuario->update($request->all());
        return $usuario;
    }

    /**
    * @OA\Delete(
    *     path="/api/usuarios/{id}",
    *     tags={"Usuarios"},
    *     summary="Eliminar usuario",
    *     @OA\Response(
    *         response=204,
    *         description="Proyecto eliminado satisfactoriamente."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function delete($id){
        $usuario = Usuario::findOrFail($id);
        $usuario->delete();
        return 204;
    }
}
