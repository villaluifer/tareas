<?php

namespace App\Http\Controllers;

use App\Models\Tarea;
use App\Models\Usuario;
use Illuminate\Http\Request;

class TareaCrudController extends Controller
{
    // Controlador ruta web

      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function index()
     {
         $tareas = Tarea::all();
         return view('tareas.index', Tarea::orderBy('id','desc')->paginate(5),compact('tareas'));
     }
 
      /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
 
     public function create()
     {
        $usuarios = Usuario::all();
        return view('tareas.create',compact('usuarios'));
     }
 
     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
 
      public function store(Request $request)
     {

        //dd($request);

         $request->validate([
             'nombre' => 'required',
             'id_usuario' => 'required',
             'descripcion' => 'required',
             'fecha_creacion' => 'required',
             'fecha_vencimiento' => 'required',
         ]);
  
         $tarea = new Tarea;
  
         $tarea->nombre = $request->nombre;
         $tarea->id_usuario = $request->id_usuario;
         $tarea->descripcion = $request->descripcion;
         $tarea->fecha_creacion = $request->fecha_creacion;
         $tarea->fecha_vencimiento = $request->fecha_vencimiento;
 
         $tarea->save();
       
         return redirect()->route('tareas.index')
                         ->with('success','La tarea ha sido creada con exito.');
     }
 
     /**
      * Display the specified resource.
      *
      * @param  \App\tarea  $tarea
      * @return \Illuminate\Http\Response
      */
 
      public function show(Tarea $tarea)
     {
        $usuarios = Usuario::all();
         return view('tareas.show',compact('tarea'),compact('usuarios'));
     }
     
      /**
      * Show the form for editing the specified resource.
      *
      * @param  \App\Tarea  $tarea
      * @return \Illuminate\Http\Response
      */
 
      public function edit(Tarea $tarea)
      {
         $usuarios = Usuario::all();
         // $this->authorize('update', $tarea);
          return view('tareas.edit',compact('tarea'), compact('usuarios'));
      }
 
      /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  \App\tarea  $tarea
      * @return \Illuminate\Http\Response
      */
 
      public function update(Request $request, $id)
     {
 
         $request->validate([
             'nombre' => 'required',
             'id_usuario' => 'required',
             'descripcion' => 'required',
             'fecha_creacion' => 'required',
             'fecha_vencimiento' => 'required',
         ]);

         $tarea = Tarea::find($id);
        
         $this->authorize('update', $tarea);

         $tarea->nombre = $request->nombre;
         $tarea->id_usuario = $request->id_usuario;
         $tarea->descripcion = $request->descripcion;
         $tarea->fecha_creacion = $request->fecha_creacion;
         $tarea->fecha_vencimiento = $request->fecha_vencimiento;
 
         $tarea->save();
         return redirect()->route('tareas.index')
                         ->with('success','La tarea ha sido actualizada con exito');
     }
 
 
      /**
      * Remove the specified resource from storage.
      *
      * @param  \App\Tarea  $tarea
      * @return \Illuminate\Http\Response
      */
 
      public function destroy(Tarea $tarea)
     {
         $tarea->delete();
      
         return redirect()->route('tareas.index')
                         ->with('success','La tarea ha sido eliminada con exito');
     }
}
