<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Http\Request;

class UsuarioCrudController extends Controller
{
    // Controlador ruta web

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function index()
     {
         $usuarios = Usuario::all();
         return view('usuarios.index', Usuario::orderBy('id','desc')->paginate(5),compact('usuarios'));
     }
 
      /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
 
     public function create()
     {
         return view('usuarios.create');
     }
 
     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
 
      public function store(Request $request)
     {
         $request->validate([
             'nombre' => 'required',
             'codigo' => 'required',
         ]);
  
         $usuario = new Usuario();
  
         $usuario->nombre = $request->nombre;
         $usuario->codigo = $request->codigo;
         $usuario->descripcion = $request->descripcion;

         $usuario->save();
  
       
         return redirect()->route('usuarios.index')
                         ->with('success','el usuario ha sido creado con exito.');
     }
 
     /**
      * Display the specified resource.
      *
      * @param  \App\Usuario  $usuario
      * @return \Illuminate\Http\Response
      */
 
      public function show(Usuario $usuario)
     {
         return view('usuarios.show',compact('usuario'));
     }
     
      /**
      * Show the form for editing the specified resource.
      *
      * @param  \App\Usuario  $usuario
      * @return \Illuminate\Http\Response
      */
 
      public function edit(Usuario $usuario)
      {
          return view('usuarios.edit',compact('usuario'));
      }
 
      /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  \App\Estudiante  $estudiante
      * @return \Illuminate\Http\Response
      */
 
      public function update(Request $request, $id)
     {
        $request->validate([
            'nombre' => 'required',
            'codigo' => 'required',
        ]);
  
         $usuario = Usuario::find($id);
  
         
         $usuario->nombre = $request->nombre;
         $usuario->codigo = $request->codigo;
         $usuario->descripcion = $request->descripcion;
 
         $usuario->save();
      
         return redirect()->route('usuarios.index')
                         ->with('success','El usuario ha sido modificado con exito');
     }
 
 
      /**
      * Remove the specified resource from storage.
      *
      * @param  \App\Usuario  $usuario
      * @return \Illuminate\Http\Response
      */
 
      public function destroy(Usuario $usuario)
     {
         $usuario->delete();
      
         return redirect()->route('usuarios.index')
                         ->with('success','El usuario ha sido eliminado con exito');
     }
}
