<?php

namespace App\Http\Controllers;

use App\Models\Etiqueta;
use Illuminate\Http\Request;

/**
* @OA\Info(
*             title="API - Tareas",
*             version="1.0",
*             description="Listado de URI'S de la API tareas"
* )
*
* @OA\Server(url="http://pttareas.test")
*/

class EtiquetaController extends Controller
{
   /**
    * @OA\SecurityScheme(
    *     type="http",
    *     description="Autenticación JWT",
    *     name="Authorization",
    *     in="header",
    *     scheme="bearer",
    *     bearerFormat="JWT",
    *     securityScheme="bearerAuth"
    * )
    */


    // Controlador Ruta Api
    
    /**
    * @OA\Get(
    *     path="/api/etiquetas",
    *     tags={"Etiquetas"},
    *     summary="Listado del registro de todos las etiquetas",
    *     @OA\Response(
    *         response=200,
    *         description="Mostrar todos los proyectos."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     ),
    *     security={
    *        {"bearerAuth": {}},
    *     },
    * )
    */

    public function index(){
        return Etiqueta::all();
    }


    /**
    * @OA\Get(
    *     path="/api/etiquetas/{id}",
    *     tags={"Etiquetas"},
    *     summary="Listado del registro de todos las etiquetas",
    *     @OA\Parameter(
    *         in="path",
    *          name="id",
    *          required=true,
    *         @OA\Schema(type="string")
   *          ),
    *     @OA\Response(
    *         response=200,
    *         description="Mostrar todos los proyectos."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     ),
    *     security={
    *        {"bearerAuth": {}},
    *     },
    * )
    */

    public function show($id){
        return Etiqueta::find($id);
    }

    /**
    * @OA\Post(
    *     path="/api/etiquetas",
    *     tags={"Etiquetas"},
    *     summary="Crear etiqueta",
    *     @OA\RequestBody(
    *         @OA\JsonContent(
    *            @OA\Property(property="id", type="number"),
    *            @OA\Property(property="nombre", type="string"),
    *            @OA\Property(property="id_tarea", type="number"),
    *            @OA\Property(property="codigo", type="string"),
    *            @OA\Property(property="descripcion", type="string")
    *         )
    *     ),
    *     @OA\Response(
    *         response=201,
    *         description="Proyecto creado satisfactoriamente."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function store(Request $request){
        Etiqueta::create($request->all());
    }
    /**
    * @OA\Put(
    *     path="/api/etiquetas/{id}",
    *     tags={"Etiquetas"},
    *     summary="Actualizar etiqueta",
    *     @OA\RequestBody(
    *         @OA\JsonContent(
    *         @OA\Property(property="id", type="number"),
    *         @OA\Property(property="nombre", type="string"),
    *         @OA\Property(property="id_tarea", type="number"),
    *         @OA\Property(property="codigo", type="string"),
    *         @OA\Property(property="descripcion", type="string")
    *         )
    *     ),
    *     @OA\Response(
    *         response=202,
    *         description="Proyecto actualizado satisfactoriamente."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function update(Request $request, $id){
        $etiqueta = Etiqueta::findOrFail($id);
        $etiqueta->update($request->all());
        return $etiqueta;
    }
    /**
    * @OA\Delete(
    *     path="/api/etiquetas/{id}",
    *     tags={"Etiquetas"},
    *     summary="Eliminar etiqueta",
    *     @OA\Response(
    *         response=204,
    *         description="Proyecto eliminado satisfactoriamente."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function delete($id){
        $etiqueta = Etiqueta::findOrFail($id);
        $etiqueta->delete();
        return 204;
    }
}
