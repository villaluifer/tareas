<?php

namespace App\Policies;

use App\Models\Tarea;
use App\Models\User;
use Illuminate\Auth\Access\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class TaskPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function update(User $user, Tarea $tarea)
    {
     return $user->id === $tarea->id_usuario;

    }

    public function delete(User $user, Tarea $tarea)
    {
        return $user->id === $tarea->id_usuario;
    }

}
