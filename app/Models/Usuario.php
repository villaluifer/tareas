<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    use HasFactory;
    protected $table = "usuarios";
    protected $primarykey = "id";
    protected $fillable = ['nombre','codigo','descripcion',
    'usuario','password_app','password','created_at','updated_at'];
}
