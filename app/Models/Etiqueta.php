<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Etiqueta extends Model
{
    use HasFactory;
    protected $table = "etiquetas";
    protected $primarykey = "id";
    protected $fillable = ['nombre','id_tarea','codigo','descripcion','created_at','updated_at'];
}
