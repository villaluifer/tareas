<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
    use HasFactory;
    protected $table = "tareas";
    protected $primarykey = "id";
    protected $fillable = ['nombre','id_usuario','descripcion',
    'fecha_creacion','fecha_vencimiento','created_at','updated_at'];
}
